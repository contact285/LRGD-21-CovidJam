﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager lm { get; private set; }

    public float levelLoadStatus = 0;
    AsyncOperation loadLevel;

    public bool wait = false;

    private void Awake()
    {
        if (lm != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            lm = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (loadLevel != null && loadLevel.progress < 0.9f)
        {
            Debug.Log("AA");
            levelLoadStatus = loadLevel.progress;
        }
        else if (loadLevel != null && loadLevel.progress == 0.9f && wait == false)
        {
            StartCoroutine(ReadyToLoad());
        }
        else if (loadLevel != null && loadLevel.progress == 1)
        {
            Debug.Log("DONE");
            loadLevel = null;
            levelLoadStatus = 0;
        }
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenuLevel");
    }

    public void RequestLoadLevel(string levelName)
    {
        //try
        //{

        //}
        //catch (UnityException e)
        //{
        //    Debug.LogError("Couldn't find a scene called ' " + levelName + "'.");
        //    Debug.LogError(e);
        //}
        //SceneManager.LoadScene(levelName);
        loadLevel = SceneManager.LoadSceneAsync(levelName);
        loadLevel.allowSceneActivation = false;
    }

    public void RequestNextLevel()
    {
        Debug.Log(SceneManager.GetActiveScene().path);
        var currentIndex = SceneUtility.GetBuildIndexByScenePath(SceneManager.GetActiveScene().path);
        if (currentIndex+1 < SceneManager.sceneCountInBuildSettings)
        {
            loadLevel = SceneManager.LoadSceneAsync(SceneUtility.GetScenePathByBuildIndex(currentIndex+1));
        }
        else
        {
            ReturnToMainMenu();
        }
    }

    public float GetLevelLoadStatus()
    {
        return levelLoadStatus;
    }

    IEnumerator ReadyToLoad()
    {
        Debug.Log("Wait!");
        wait = true;
        yield return new WaitForSeconds(1f);
        if (loadLevel != null) loadLevel.allowSceneActivation = true;
        wait = false;
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Debug.Log("Quitting!");
        Application.Quit();
    }
}
