﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelSelectButton : MonoBehaviour
{
    public TMP_Text buttonText;

    string levelPath;
    int levelNumber;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLevelPath(string path)
    {
        levelPath = path;
    }

    public void SetLevelNumber(int number)
    {
        levelNumber = number;
        buttonText.text = levelNumber.ToString();
    }

    public void RequestLoad()
    {
        LevelManager.lm.RequestLoadLevel(levelPath);
    }
}
