﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject menuButtonUI;
    public GameObject levelSelectUI;
    public GameObject settingsUI;

    public Transform levelSelectParent;
    public GameObject levelSelectButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleLevelSelectUI()
    {
        menuButtonUI.SetActive(false);
        levelSelectUI.SetActive(true);
        settingsUI.SetActive(false);
    }

    public void ToggleSettingsUI()
    {
        menuButtonUI.SetActive(false);
        levelSelectUI.SetActive(false);
        settingsUI.SetActive(true);
    }

    public void ToggleMenuButtonUI()
    {
        menuButtonUI.SetActive(true);
        levelSelectUI.SetActive(false);
        settingsUI.SetActive(false);
    }

    public void PopulateLevelSelectButtons()
    {
        for (int i=1; i<SceneManager.sceneCountInBuildSettings; i++)
        {
            Debug.Log(SceneUtility.GetScenePathByBuildIndex(i));
            var newButton = Instantiate(levelSelectButton, levelSelectParent);
            newButton.GetComponent<LevelSelectButton>().SetLevelPath(SceneUtility.GetScenePathByBuildIndex(i));
            newButton.GetComponent<LevelSelectButton>().SetLevelNumber(i);
        }
    }

    public void Quit()
    {
        LevelManager.lm.Quit();
    }

}
