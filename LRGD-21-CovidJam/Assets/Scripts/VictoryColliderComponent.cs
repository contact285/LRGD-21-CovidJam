﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryColliderComponent : MonoBehaviour
{
    public List<GameObject> inside = new List<GameObject>();

    public PuzzleManager pm;

    bool checking = false;
    bool won = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (inside.Count == 0 && checking == false && won == false)
        {
            checking = true;
            StartCoroutine(CheckEmpty());
        }
    }

    IEnumerator CheckEmpty()
    {
        Debug.Log("Empty?");
        yield return new WaitForSeconds(3);
        if (inside.Count == 0)
        {
            Debug.Log("Win");
            won = true;
            pm.EnableVictoryMenu();
        }
        else Debug.Log("Not yet");
        checking = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        inside.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        inside.Remove(other.gameObject);
    }
}
