﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class PlayerMouseInput : MonoBehaviour
{
    Camera cam;
    //public RectTransform mouseTest;

    public enum CurrentTool
    {
        EXPLOSION = 0,
        ACTIVATION = 1,
        SUMMON = 2,
        ERASE = 3
    }

    public CurrentTool tool = CurrentTool.EXPLOSION;

    public List<GameObject> summonableObjects = new List<GameObject>();
    public int selectedSummonable = 0;

    public int explosionForce; // Newtons
    public float explosionRadius; // Meters

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector2 mousePos = Mouse.current.position.ReadValue();
        //mouseTest.transform.position = new Vector2(mousePos.x - mouseTest.rect.width / 2, mousePos.y + mouseTest.rect.height / 2);
    }

    public void Interact(InputAction.CallbackContext ctx)
    {
        //Debug.Log(ctx.started);
        //Debug.Log(ctx.ReadValue<float>());

        if (ctx.started == true)
        {
            // Only register click if mouse is not over a UI gameobject
            if (EventSystem.current.IsPointerOverGameObject() == false)
            {
                //Debug.Log("Click!");
                var worldPoint = cam.ScreenToWorldPoint(Mouse.current.position.ReadValue());
                //Debug.Log(worldPoint);
                RaycastHit hit;
                Physics.Raycast(worldPoint, cam.transform.forward, out hit, 50f);
                Debug.DrawRay(worldPoint, cam.transform.forward * 50, Color.magenta, 10);

                // Use appropriate function for current tool
                switch (tool)
                {
                    case CurrentTool.EXPLOSION:
                        ApplyExplosion(hit);
                        break;
                    case CurrentTool.ACTIVATION:
                        ApplyActivation(hit);
                        break;
                    case CurrentTool.SUMMON:
                        ApplySummon(hit);
                        break;
                    case CurrentTool.ERASE:
                        ApplyErase(hit);
                        break;
                    default:
                        Debug.LogError("Unrecognized tool:" + tool);
                        break;
                }
            }
        }
        else if (ctx.canceled == true)
        {
            //Debug.Log("Lift");
        }
    }

    public void SelectTool(string toolName)
    {
        CurrentTool selectedTool;
        if (Enum.TryParse(toolName, out selectedTool))
        {
            Debug.Log(selectedTool);
            tool = selectedTool;
        }
        else
        {
            Debug.LogError("Unknown tool " + toolName + " selected.");
        }
    }

    void ApplyExplosion(RaycastHit hit)
    {
        if (hit.collider != null)
        {
            // Check if currently selected tool has charges AFTER we hit anything
            //if (toolCharges[(int)tool] > 0)
            if (PuzzleManager.pm.GetAvailableCharges((int)tool) == true)
            {
                Collider[] victims = Physics.OverlapSphere(hit.point, explosionRadius);
                foreach (Collider victim in victims)
                {
                    Rigidbody rb = victim.GetComponent<Rigidbody>();
                    if (rb != null) rb.AddExplosionForce(explosionForce, hit.point, explosionRadius);
                }
                //toolCharges[(int)tool]--;
                PuzzleManager.pm.DeductCharge((int)tool);
            }
        }
    }

    void ApplyActivation(RaycastHit hit)
    {
        if (hit.collider != null && hit.transform.GetComponent<Activations>() != null)
        {
            // Check if currently selected tool has charges AFTER we've hit something activatable
            if (PuzzleManager.pm.GetAvailableCharges((int)tool) == true)
            {
                hit.transform.GetComponent<Activations>().Activate();
                PuzzleManager.pm.DeductCharge((int)tool);
            }
        }
    }

    void ApplySummon(RaycastHit hit)
    {
        if (hit.collider != null)
        {
            // Check if currently selected tool has charges AFTER we've hit something
            if (PuzzleManager.pm.GetAvailableCharges((int)tool) == true)
            {
                // Summon the currently selected object
                Instantiate(summonableObjects[selectedSummonable], new Vector3(hit.point.x, hit.point.y + 5f, hit.point.z), Quaternion.identity);
                PuzzleManager.pm.DeductCharge((int)tool);
            }
        }
    }

    void ApplyErase(RaycastHit hit)
    {
        if (hit.collider != null && hit.transform.gameObject.layer == LayerMask.NameToLayer("Erasable"))
        {
            if (PuzzleManager.pm.GetAvailableCharges((int)tool) == true)
            {
                // Immediately destroy object
                Destroy(hit.transform.gameObject);
                PuzzleManager.pm.DeductCharge((int)tool);
            }
        }
    }
}
