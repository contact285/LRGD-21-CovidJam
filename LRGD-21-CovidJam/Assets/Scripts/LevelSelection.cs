﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour
{
    public Slider loadSlider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        loadSlider.value = LevelManager.lm.GetLevelLoadStatus();
    }

    public void TestGoToLevel()
    {
        LevelManager.lm.RequestLoadLevel("TemplateLevel");
    }
}
