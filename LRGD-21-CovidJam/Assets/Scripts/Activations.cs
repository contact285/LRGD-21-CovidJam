﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activations : MonoBehaviour
{
    public enum ActivationType
    {
        DESTROY,
        EXPLODE,
        ANIMATE
    }

    public ActivationType currentType = ActivationType.DESTROY;

    public int destroyDelay = 0; // in seconds

    public int explodeCount = 2;

    public string animTrigger;

    public List<Animator> anims = new List<Animator>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activate()
    {
        switch(currentType)
        {
            case ActivationType.DESTROY:
                Destroy();
                break;
            case ActivationType.EXPLODE:
                Explode();
                break;
            case ActivationType.ANIMATE:
                Animate();
                break;
        }
    }

    void Destroy()
    {
        Destroy(this.gameObject, destroyDelay);
    }

    void Explode()
    {
        for (int i = 0; i < explodeCount; i++) Debug.Log("EXPLODE");
    }
    void Animate()
    {
        //GetComponent<Animator>().SetTrigger(animTrigger);
        foreach(Animator anim in anims)
        {
            anim.SetTrigger(animTrigger);
        }
    }
}