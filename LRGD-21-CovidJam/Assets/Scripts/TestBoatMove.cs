﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBoatMove : MonoBehaviour
{
    public PuzzleManager pm;
    public float moveSpeed;
    Rigidbody rb;
    bool move = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (move == true)
        {
            rb.velocity = new Vector3(moveSpeed, rb.velocity.y, rb.velocity.z);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bridge")
        {
            move = false;
            pm.EnableVictoryMenu();
        }
    }
}
