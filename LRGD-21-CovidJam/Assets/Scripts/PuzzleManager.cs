﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class PuzzleManager : MonoBehaviour
{
    public static PuzzleManager pm { get; private set; }

    [SerializeField]
    List<int> toolCharges = new List<int>() { 0, 0, 0, 0 };

    [SerializeField]
    List<TMP_Text> chargeButtonTexts = new List<TMP_Text>();

    public Button firstTool;

    bool canInteractWithScene = true;
    public GameObject pauseMenuHolder;

    public GameObject victoryMenuHolder;

    private void Awake()
    {
        if (pm != null)
        {
            Destroy(this.gameObject);
        }
        else pm = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Initialize button text for tool charges
        for(int i=0; i<toolCharges.Count; i++)
        {
            UpdateChargeText(i);
        }
        firstTool.Select();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool GetAvailableCharges(int slot)
    {
        return toolCharges[slot] > 0;
    }

    public void DeductCharge(int slot)
    {
        toolCharges[slot]--;
        UpdateChargeText(slot);
    }

    void UpdateChargeText(int slot)
    {
        chargeButtonTexts[slot].text = toolCharges[slot].ToString();
    }

    public void PauseInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started == true)
        {
            TogglePauseMenu();
        }
    }

    public void TogglePauseMenu()
    {
        canInteractWithScene = !canInteractWithScene;
        if (canInteractWithScene == false)
        {
            // show the pause menu
            pauseMenuHolder.SetActive(true);
            canInteractWithScene = false;
        }
        else
        {
            // hide the pause menu
            pauseMenuHolder.SetActive(false);
            canInteractWithScene = true;
        }
    }

    public void EnableVictoryMenu()
    {
        pauseMenuHolder.SetActive(false);
        victoryMenuHolder.SetActive(true);
        canInteractWithScene = false;
    }

    public bool CheckSceneInteractable()
    {
        return canInteractWithScene;
    }

    public void QuitToMainMenu()
    {
        LevelManager.lm.ReturnToMainMenu();
    }

    public void Reset()
    {
        LevelManager.lm.ResetLevel();
    }

    public void ProceedToNextLevel()
    {
        LevelManager.lm.RequestNextLevel();
    }

    public void Quit()
    {
        Debug.Log("Quitting from pause");
        LevelManager.lm.Quit();
    }
}
