﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Activations)), CanEditMultipleObjects]
public class ActivationEditor : Editor
{
    public SerializedProperty
        currentType_Prop,
        destroyDelay_Prop,
        explodeCount_Prop,
        animTrigger_Prop,
        anims_Prop;

    private void OnEnable()
    {
        currentType_Prop = serializedObject.FindProperty("currentType");
        destroyDelay_Prop = serializedObject.FindProperty("destroyDelay");
        explodeCount_Prop = serializedObject.FindProperty("explodeCount");
        animTrigger_Prop = serializedObject.FindProperty("animTrigger");
        anims_Prop = serializedObject.FindProperty("anims");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(currentType_Prop);
        // PropertyHolder.Status st = (PropertyHolder.Status)state_Prop.enumValueIndex;
        Activations.ActivationType type = (Activations.ActivationType)currentType_Prop.enumValueIndex;

        switch (type)
        {
            case Activations.ActivationType.DESTROY:
                EditorGUILayout.PropertyField(destroyDelay_Prop, new GUIContent("Destroy Delay (sec)"));
                break;
            case Activations.ActivationType.EXPLODE:
                EditorGUILayout.PropertyField(explodeCount_Prop, new GUIContent("Explode Count"));
                break;
            case Activations.ActivationType.ANIMATE:
                EditorGUILayout.PropertyField(animTrigger_Prop, new GUIContent("Animation Trigger"));
                EditorGUILayout.PropertyField(anims_Prop, new GUIContent("Animators"));
                break;
        }    

        serializedObject.ApplyModifiedProperties();
    }
}