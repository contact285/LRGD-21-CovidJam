﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerMouseInput)), CanEditMultipleObjects]
public class PlayerMouseInputEditor : Editor
{
    public SerializedProperty
        tool_Prop,
        toolCharges_Prop,
        summonableObjects_Prop,
        selectedSummonable_Prop,
        explosionForce_Prop,
        explosionRadius_Prop;

    private void OnEnable()
    {
        tool_Prop = serializedObject.FindProperty("tool");
        toolCharges_Prop = serializedObject.FindProperty("toolCharges");
        summonableObjects_Prop = serializedObject.FindProperty("summonableObjects");
        selectedSummonable_Prop = serializedObject.FindProperty("selectedSummonable");
        explosionForce_Prop = serializedObject.FindProperty("explosionForce");
        explosionRadius_Prop = serializedObject.FindProperty("explosionRadius");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        //EditorGUILayout.PropertyField(toolCharges_Prop);

        EditorGUILayout.PropertyField(tool_Prop);
        PlayerMouseInput.CurrentTool ct = (PlayerMouseInput.CurrentTool)tool_Prop.enumValueIndex;

        switch(ct)
        {
            case PlayerMouseInput.CurrentTool.EXPLOSION:
                EditorGUILayout.PropertyField(explosionForce_Prop, new GUIContent("Force"));
                EditorGUILayout.PropertyField(explosionRadius_Prop, new GUIContent("Radius"));
                break;
            case PlayerMouseInput.CurrentTool.ACTIVATION:
                //
                break;
            case PlayerMouseInput.CurrentTool.SUMMON:
                EditorGUILayout.PropertyField(summonableObjects_Prop, new GUIContent("Objects"));
                EditorGUILayout.PropertyField(selectedSummonable_Prop, new GUIContent("Selected ID"));
                break;
            case PlayerMouseInput.CurrentTool.ERASE:
                //
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
