﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuObjectCatcher : MonoBehaviour
{
    public MenuObjectSpawner MOS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.transform.root.name);
        other.transform.GetComponent<Rigidbody>().velocity = Vector3.zero;
        MOS.ReturnObjectToPool(other.transform.root.gameObject);
    }
}
