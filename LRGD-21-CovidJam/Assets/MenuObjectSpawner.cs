﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuObjectSpawner : MonoBehaviour
{
    public List<GameObject> spawnObjects;
    public List<GameObject> sceneObjects;
    public float spawnDelay;

    public Transform spawnPosition;

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject obj in spawnObjects)
        {
            var newObj = Instantiate(obj, spawnPosition.position, Quaternion.identity);
            newObj.SetActive(false);
            sceneObjects.Add(newObj);
        }
        StartCoroutine(SpawnObject());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReturnObjectToPool(GameObject spawnedObject)
    {
        if (!sceneObjects.Contains(spawnedObject))
        {
            Debug.LogError("Attempted to return object not in spawned list! This is a main menu bug!");
        }
        else
        {
            spawnedObject.SetActive(false);
            spawnedObject.transform.position = spawnPosition.position;
        }
    }

    IEnumerator SpawnObject()
    {
        if (spawnDelay > 0)
        {
            yield return new WaitForSeconds(spawnDelay);
            if (CheckIfObjectsDisabled() == true)
            {
                var index = Random.Range(0, sceneObjects.Count - 1);
                if (sceneObjects[index].activeSelf == false)
                {
                    sceneObjects[index].SetActive(true);
                    sceneObjects[index].transform.rotation = UnityEngine.Random.rotation;
                    //break;
                }
            }
            StartCoroutine(SpawnObject());
        }
    }

    bool CheckIfObjectsDisabled()
    {
        bool anyDisabled = false;
        foreach(GameObject obj in sceneObjects)
        {
            if (obj.activeInHierarchy == false) anyDisabled = true;
        }
        return anyDisabled;
    }
}
